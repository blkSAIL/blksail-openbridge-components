import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class BarTicksMainEngine extends Component {
  render() {
    if (this.props.sml === 'm') {
      return (
        <svg
          x={this.props.x}
          y={this.props.y}
          width="146"
          height="39"
          viewBox="0 0 146 39"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <line x1="1" y1="38.5" x2="0.999998" y2="0.5" className="blksail-border-divider-color" strokeOpacity="0.3" />
          <line x1="25" y1="38.5" x2="25" y2="0.5" className="blksail-border-divider-color" strokeOpacity="0.3" />
          <line x1="97" y1="38.5" x2="97" y2="0.5" className="blksail-border-divider-color" strokeOpacity="0.3" />
          <line x1="49" y1="38.5" x2="49" y2="0.5" className="blksail-border-divider-color" strokeOpacity="0.3" />
          <line x1="121" y1="38.5" x2="121" y2="0.5" className="blksail-border-divider-color" strokeOpacity="0.3" />
          <line x1="73" y1="38.5" x2="73" y2="0.5" className="blksail-border-divider-color" strokeOpacity="0.3" />
          <line x1="145" y1="38.5" x2="145" y2="0.5" className="blksail-border-divider-color" strokeOpacity="0.3" />
        </svg>
      );
    } else if (this.props.sml === 'l') {
      return (
        <svg
          x={this.props.x}
          y={this.props.y}
          width="80"
          height="384"
          viewBox="0 0 80 384"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path d="M80 192L4.22597e-05 192" className="blksail-border-divider-color" strokeOpacity="0.3" />
          <path d="M80 240L3.8445e-05 240" className="blksail-border-divider-color" strokeOpacity="0.3" />
          <path d="M80 288L3.8445e-05 288" className="blksail-border-divider-color" strokeOpacity="0.3" />
          <path d="M80 336L3.46303e-05 336" className="blksail-border-divider-color" strokeOpacity="0.3" />
          <path d="M80 144L4.22597e-05 144" className="blksail-border-divider-color" strokeOpacity="0.3" />
          <path d="M80 96L4.60744e-05 96" className="blksail-border-divider-color" strokeOpacity="0.3" />
          <path d="M80 48L4.60744e-05 48" className="blksail-border-divider-color" strokeOpacity="0.3" />
        </svg>
      );
    } else {
      return '';
    }
  }
}

BarTicksMainEngine.defaultProps = {};

BarTicksMainEngine.propTypes = {
    x: PropTypes.number,
    y: PropTypes.number,
    sml: PropTypes.string,
    /**
     * Dash-assigned callback that should be called to report property changes
     * to Dash, to make them available for callbacks.
     */
    setProps: PropTypes.func
};
