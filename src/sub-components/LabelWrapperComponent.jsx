import React, { Component } from 'react';
import LabelComponent from './LabelComponent.jsx';
import PropTypes from 'prop-types';
import { lab } from 'd3';


export default class LabelWrapperComponent extends Component {
  render() {
    const { showLabel, sml, labelInfos, component, labelRowPriority } = this.props;
    let flexDirectionPrimary = 'row';
    let flexDirectionSecondary = 'column';
    if (!labelRowPriority) {
      flexDirectionPrimary = 'column';
      flexDirectionSecondary = 'row';
    }

    return (
      <div
        style={{
          height: '100%',
          width: '100%',
          display: 'flex',
          flexWrap: 'wrap',
          flexDirection: flexDirectionPrimary,
          justifyContent: 'space-around',
          alignContent: 'center',
        }}
      >
        <div>{component}</div>

        {labelInfos.map((labelInfoArray, rowIndex) => {
          return (
            showLabel && (
              <div
                key={ rowIndex}
                style={{
                  display: 'flex',
                  flexWrap: 'wrap',
                  flexDirection: flexDirectionSecondary,
                  justifyContent: 'center',
                }}
              >
                {labelInfoArray.map((labelInfo, labelIndex) => {
                  return (
                    <div key={ rowIndex + ' ' + labelIndex}>
                      <LabelComponent
                        label_text={labelInfo.title}
                        value={labelInfo.value}
                        unit_text={labelInfo.unit}
                        isRequestedValue={labelInfo.isRequestedValue}
                        sml={sml}
                      />
                    </div>
                  );
                })}
              </div>
            )
          );
        })}
      </div>
    );
  }
}

LabelWrapperComponent.defaultProps = {};

LabelWrapperComponent.propTypes = {

    showLabel: PropTypes.bool,

    sml: PropTypes.string,
    component: PropTypes.node,
    labelInfos: PropTypes.array,
    labelRowPriority: PropTypes.bool,


    /**
     * Dash-assigned callback that should be called to report property changes
     * to Dash, to make them available for callbacks.
     */
    setProps: PropTypes.func
};
