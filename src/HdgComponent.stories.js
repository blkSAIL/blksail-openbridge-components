import React from 'react';

import HdgComponent from './HdgComponent';

export default {
    title: 'Heading Component',
    component: HdgComponent,
    argTypes: {
        heading_degrees: {
            name: "Heading (degrees)",
            control: {
                type: 'range',
                min: -360,
                max: 360,
                step: 1
            }
        },
        rotate_frame: {
            name: "Rotate frame",
        },
        sml: {
            name: "Component Size",
            control: {
                type: 'select',
                options: ['s', 'm', 'l']
            }
        }
    },
};

const Template = (args) => <HdgComponent {...args} />;

export const Large = Template.bind({});
Large.args = {
    heading_degrees: 15,
    rotate_frame: false,
    sml: 'l',
};

export const LargeRotateFrame = Template.bind({});
LargeRotateFrame.args = {
    heading_degrees: 15,
    rotate_frame: true,
    sml: 'l',
};

export const Medium = Template.bind({});
Medium.args = {
    heading_degrees: 15,
    rotate_frame: false,
    sml: 'm',
};

export const Small = Template.bind({});
Small.args = {
    heading_degrees: 15,
    rotate_frame: false,
    sml: 's',
};
