import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ArrowSvgComponent from './svg-components/ArrowSvgComponent.jsx';
import BarPassiveSvgComponent from './svg-components/BarPassiveSvgComponent.jsx';
import BarActiveSvgComponent from './svg-components/BarActiveSvgComponent.jsx';
import BarTickFrameSvgComponent from './svg-components/BarTickFrameSvgComponent.jsx';
import LineSvgComponent from './svg-components/LineSvgComponent.jsx';
import BarTicksMainEngine from './sub-components/BarTicksMainEngine.jsx';
import * as d3 from 'd3';
import InterBarTicksMainEngine from './sub-components/InterBarTicksMainEngine.jsx';

export default class MainEngineComponent extends Component {
    render() {
        const { x, y, requested_throttle, engine_throttle, rotation_degrees, sml } = this.props;
        let bar_passive_height = 192;
        let bar_passive_width = 24;
        let total_width = 40;
        let total_height = 256;
        let arrow_padding_x = 12;
        let arrow_padding_y = 0;
        let bar_padding_x = 8;
        let bar_padding_y = 24;
        let bar_ticks_padding_x = -27;
        let bar_ticks_padding_y = 76.5;
        if (sml === 's') {
            bar_passive_height = 64;
            bar_passive_width = 8;
            total_width = 16;
            total_height = 80;
            arrow_padding_x = 5;
            bar_padding_x = 4;
            bar_padding_y = 8;
        } else if (sml === 'l') {
            bar_passive_height = 384;
            bar_passive_width = 48;
            total_width = 80;
            total_height = 512;
            arrow_padding_x = 25;
            arrow_padding_y = -2;
            bar_padding_x = 16;
            bar_padding_y = 48;
            bar_ticks_padding_x = 0;
            bar_ticks_padding_y = 48;
        }
        let scale = d3
            .scaleLinear()
            .domain([-100, 100])
            .range([bar_passive_height, 0]);
        let bar_passive_center = bar_passive_height / 2;
        let engine_throttle_bar_end_pixels = scale(engine_throttle);
        let requested_throttle_height_pixels = scale(requested_throttle);
        let pixel_difference = engine_throttle_bar_end_pixels - bar_passive_center;
        let active_bar_height = 0;
        let active_bar_y = 0;

        if (pixel_difference < 0) {
            active_bar_y = engine_throttle_bar_end_pixels;
            active_bar_height = -pixel_difference;
        } else {
            active_bar_y = bar_passive_center;
            active_bar_height = pixel_difference;
        }

        let transform_string =
            'rotate(' + rotation_degrees + ' ' + total_width / 2 + ' ' + (bar_passive_center + bar_padding_y) + ')';
        let rotate_barticks_string = 'rotate(90 ' + total_width / 2 + ' ' + bar_passive_center + ')';
        if (sml === 'l') {
            //Use ticks of main engine and not bow thruster, no need to rotate them!
            rotate_barticks_string = 'rotate(0 0 0)';
        }
        let viewBox = '0 0 ' + total_width + ' ' + total_height;
        console.log('rotation string: ' + transform_string);

        return (
            <svg
                x={x}
                y={y}
                className={'blksail-border-outline-stroke'}
                width={total_height}
                height={total_height}
                viewBox={viewBox}
            >
                <g transform={transform_string}>
                    <BarTickFrameSvgComponent x={0} y={bar_padding_y} sml={sml} />
                    <g transform={rotate_barticks_string}>
                        <BarTicksMainEngine x={bar_ticks_padding_x} y={bar_ticks_padding_y} sml={sml} />
                        <InterBarTicksMainEngine x={bar_ticks_padding_x} y={bar_ticks_padding_y} sml={sml} />
                    </g>
                    <ArrowSvgComponent x={arrow_padding_x} y={arrow_padding_y} fillClassName={'blksail-dynamic'} sml={sml} />
                    <BarPassiveSvgComponent
                        x={bar_padding_x}
                        y={bar_padding_y}
                        fillClassNameBorder={'blksail-border-outline'}
                        sml={sml}
                    />
                    <BarActiveSvgComponent
                        x={bar_padding_x}
                        y={bar_padding_y + active_bar_y}
                        bar_width={bar_passive_width}
                        bar_height={active_bar_height}
                        sml={sml}
                    />
                    <LineSvgComponent x={0} y={bar_passive_center + bar_padding_y} fillClassName={'blksail-dynamic'} sml={sml} />
                    <LineSvgComponent
                        x={0}
                        y={bar_padding_y + requested_throttle_height_pixels}
                        fillClassName={'blksail-input'}
                        sml={sml}
                    />
                </g>
            </svg>
        );
    }
}
MainEngineComponent.defaultProps = {};

MainEngineComponent.propTypes = {
    x: PropTypes.number,
    y: PropTypes.number,
    requested_throttle: PropTypes.number,
    engine_throttle: PropTypes.number,
    rotation_degrees: PropTypes.number,
    sml: PropTypes.string,
    /**
     * Dash-assigned callback that should be called to report property changes
     * to Dash, to make them available for callbacks.
     */
    setProps: PropTypes.func
};
