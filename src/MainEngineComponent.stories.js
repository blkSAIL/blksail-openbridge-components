import React from 'react';

import MainEngineComponent from "./MainEngineComponent";

export default {
    title: 'Main Engine Component',
    component: MainEngineComponent,
    argTypes: {
        requested_throttle : {
            name: "Requested Throttle (%)",
            control: {
                type: 'range',
                min: -100,
                max: 100,
                step: 1
            }
        },
        engine_throttle: {
            name: "Engine Power (%)",
            control: {
                type: 'range',
                min: -100,
                max: 100,
                step: 1
            }
        },
        rotation_degrees: {
            name: "Component Rotation (optional)",
            control: {
                type: 'range',
                min: 0,
                max:360,
                step:90
            }
        },
        sml: {
            name: "Component Size",
            control: {
                type: 'select',
                options: ['s', 'm', 'l']
            }
        }
    },
};

const Template = (args) => <MainEngineComponent {...args} />;

export const Example = Template.bind({});
Example.args = {
    x: 0,
    y: 0,
    requested_throttle: 20,
    engine_throttle: 15,
    rotation_degrees: 0,
    sml: 'l',
};
