import React, {Component} from 'react';
import PropTypes from 'prop-types';

/**
 * Displays rate of turn
 */
export default class RotSvgComponent extends Component {

    render() {
        const {x, y, setProps} = this.props;
        return (
                <div>
                    <svg x={x} y={y} width="256" height="256" viewBox="0 0 256 256" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <g transform="rotate(0 128 128)">
                            <circle cx="128" cy="22" r="8" className="blksail-dynamic" />
                            <circle cx="27.188" cy="95.2442" r="8" transform="rotate(-72 27.188 95.2442)" className="blksail-dynamic" />
                            <circle cx="65.6948" cy="213.756" r="8" transform="rotate(-144 65.6948 213.756)" className="blksail-dynamic" />
                            <circle cx="190.305" cy="213.756" r="8" transform="rotate(144 190.305 213.756)" className="blksail-dynamic" />
                            <circle cx="228.812" cy="95.2442" r="8" transform="rotate(72 228.812 95.2442)" className="blksail-dynamic" />
                        </g>
                        <circle cx="128" cy="128" r="120" className="blksail-container-section-stroke" strokeWidth="8" />
                        <circle cx="128" cy="128" r="123.5" className="blksail-border-divider-color" strokeOpacity="0.3" />
                        <circle cx="128" cy="128" r="116.5" className="blksail-border-divider-color" strokeOpacity="0.12" />
                        <circle cx="128" cy="128" r="96" className="blksail-container-section-fill" />
                        <circle cx="128" cy="128" r="95.5" className="blksail-border-divider-color" strokeOpacity="0.12" />
                    </svg>
                </div>
        );
    }
}

RotSvgComponent.defaultProps = {};

RotSvgComponent.propTypes = {
    /**
     * The ID used to identify this component in Dash callbacks.
     */
    x: PropTypes.number,

    /**
     * A label that will be printed when this component is rendered.
     */
    y: PropTypes.number,
    //rot_degrees_min: PropTypes.number,

    /**
     * Dash-assigned callback that should be called to report property changes
     * to Dash, to make them available for callbacks.
     */
    setProps: PropTypes.func
};
