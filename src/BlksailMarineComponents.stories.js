import React from 'react';

import BlksailMarineComponents from './BlksailMarineComponents';

export default {
    title: 'blkSAIL Marine Components',
    component: BlksailMarineComponents,
    argTypes: {
        realValue: {
            name: "Sensor reading",
            control: {
                type: 'range',
                min: -360,
                max: 360,
                step: 1
            }
        },
        requestedValue: {
            name: "Input",
            control: {
                type: 'range',
                min: -360,
                max: 360,
                step: 1
            }
        },
        realAzimuthAngle : {
            name: "Azimuth Angle",
            control: {
                type: 'range',
                min: -360,
                max: 360,
                step: 1
            }
        },
        requestedAzimuthAngle: {
            name: "Input Azimuth Angle",
            control: {
                type: 'range',
                min: -360,
                max: 360,
                step: 1
            }
        },
        sml: {
            name: "Component Size",
            control: {
                type: 'select',
                options: ['s', 'm', 'l']
            }
        },
        pluginType: {
            name: "Component Type",
            control: {
                type: 'select',
                options: ['rot', 'rudder', 'engine', 'hdg_compass', 'thruster', 'azimuth', ]
            }
        },
        showLabel: {
            name: "Show Label",
        },
        rotateFrame: {
            name: "Rotate Frame",
            control: {
                type: 'boolean',
            }
        },
        setProps: {
            table: {disable: true},
        },
        id: {
            table: { disable: true},
        },
    },
};

const Template = (args) => <BlksailMarineComponents {...args} />;

export const Large = Template.bind({});
Large.args = {
    realValue: 15,
    requestedValue: 20,
    realAzimuthAngle: 30,
    requestedAzimuthAngle: 25,
    maxAngleRudder: 90,
    sml: 'l',
    pluginType: 'azimuth',
};