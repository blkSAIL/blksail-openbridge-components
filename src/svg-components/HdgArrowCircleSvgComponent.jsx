import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class HdgArrowCircleSvgComponent extends Component {
  render() {
    if (this.props.sml === 'l') {
      return (
        <svg
          x={this.props.x}
          y={this.props.y}
          width="16"
          height="16"
          viewBox="0 0 16 16"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <circle cx="8" cy="8" r="8" className="blksail-dynamic" />
        </svg>
      );
    }
    return <div></div>;
  }
}

HdgArrowCircleSvgComponent.defaultProps = {};

HdgArrowCircleSvgComponent.propTypes = {
    x: PropTypes.number,
    y: PropTypes.number,
    sml: PropTypes.string,
    /**
     * Dash-assigned callback that should be called to report property changes
     * to Dash, to make them available for callbacks.
     */
    setProps: PropTypes.func
};
