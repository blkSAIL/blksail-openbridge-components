import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class BarTickFrameSvgComponent extends Component {
    render() {
        const { x, y, sml } = this.props;
        if (sml === 'm') {
            return (
                <svg x={x} y={y} width="40" height="192" viewBox="0 0 40 192" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path
                        fillRule="evenodd"
                        clipRule="evenodd"
                        d="M0 190.12V1.87997C6.46492 0.646932 13.1529 -1.90735e-06 20 -1.90735e-06C26.8471 -1.90735e-06 33.5351 0.646933 40 1.87997V190.12C33.5351 191.353 26.8471 192 20 192C13.1529 192 6.46492 191.353 0 190.12Z"
                        className={'blksail-container-background-fill'}
                    />
                </svg>
            );
        } else if (sml === 'l') {
            return (
                <svg x={x} y={y} width="80" height="384" viewBox="0 0 80 384" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path
                        fillRule="evenodd"
                        clipRule="evenodd"
                        d="M3.22736e-05 379.827L5.31736e-05 4.17262C12.9024 1.43841 26.2832 1.4623e-06 40.0001 2.22545e-06C53.7169 2.9886e-06 67.0978 1.43841 80.0001 4.17262L80 379.827C67.0977 382.562 53.7169 384 40 384C26.2832 384 12.9023 382.562 3.22736e-05 379.827Z"
                        className={'blksail-container-background-fill'}
                    />
                </svg>
            );
        } else {
            return <div>Received an unsupported size!</div>;
        }
    }
}

BarTickFrameSvgComponent.defaultProps = {};
BarTickFrameSvgComponent.propTypes = {
    x: PropTypes.number,
    y: PropTypes.number,
    sml: PropTypes.string,
    /**
     * Dash-assigned callback that should be called to report property changes
     * to Dash, to make them available for callbacks.
     */
    setProps: PropTypes.func
};
