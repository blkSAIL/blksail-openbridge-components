import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class RudderInnerCircleSvgComponent extends Component {
    render() {
        const { x, y, radius } = this.props;
        return <circle cx={x} cy={y} r={radius} className="blksail-container-section-fill" />;
    }
}

RudderInnerCircleSvgComponent.defaultProps = {};

RudderInnerCircleSvgComponent.propTypes = {
    x: PropTypes.number,
    y: PropTypes.number,
    radius: PropTypes.number,
    /**
     * Dash-assigned callback that should be called to report property changes
     * to Dash, to make them available for callbacks.
     */
    setProps: PropTypes.func
};
