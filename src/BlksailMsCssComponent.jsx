import React, {Component} from 'react';
import PropTypes from 'prop-types';
import '../styles/openbridge.css';
import './blksail.css'

export default class BlksailMsCssComponent extends Component {
    render() {
        const {theme, setProps} = this.props;
        if (theme === 'day') {
            require('../styles/openbridge-day.css');
        } else if(theme === 'bright') {
            require('../styles/openbridge-bright.css');
        } else if(theme === 'dusk') {
            require('../styles/openbridge-dusk.css');
        } else if(theme === 'night') {
            require('../styles/openbridge-night.css');
        }
        return (
            <div>

            </div>
        );
    }
}

BlksailMsCssComponent.defaultProps = {};

BlksailMsCssComponent.propTypes = {
    /**
     * Theme of the components
     */
    theme: PropTypes.string,
    /**
     * Dash-assigned callback that should be called to report property changes
     * to Dash, to make them available for callbacks.
     */
    setProps: PropTypes.func
};