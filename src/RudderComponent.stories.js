import React from 'react';

import MainEngineComponent from "./MainEngineComponent";
import RudderComponent from "./RudderComponent";
import PropTypes from "prop-types";

export default {
    title: 'Rudder Component',
    component: RudderComponent,
    argTypes: {
        rudder_angle_degrees: {
            name: "Rudder Angle (degrees)",
            control: {
                type: 'range',
                min: -90,
                max: 90,
                step: 1
            }
        },
        requested_rudder_angle_degrees: {
            name: "Requested Rudder Angle (degrees)",
            control: {
                type: 'range',
                min: -90,
                max: 90,
                step: 1
            }
        },
        maxAngleDegrees : {
            name: "Maximum Rudder Angle (degrees)",
            control: {
                type: 'range',
                min: 0,
                max: 90,
                step: 1
            }
        },
        sml: {
            name: "Component Size",
            control: {
                type: 'select',
                options: ['s', 'm', 'l']
            }
        }
    },
};

const Template = (args) => <RudderComponent {...args} />;

export const Example = Template.bind({});
Example.args = {
    rudder_angle_degrees: 5,
    requested_rudder_angle_degrees: 10,
    maxAngleDegrees: 20,
    sml: 'l',
};
