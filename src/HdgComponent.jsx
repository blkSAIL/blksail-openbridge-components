import React, { Component } from 'react';
import PropTypes from 'prop-types';

import HdgArrowSvgComponent from './svg-components/HdgArrowSvgComponent.jsx';
import AzimuthWatchFaceSvgComponent from './svg-components/AzimuthWatchFaceSvgComponent.jsx';
import HdgNorthArrowSvgComponent from './svg-components/HdgNorthArrowSvgComponent.jsx';
import HdgArrowCircleSvgComponent from './svg-components/HdgArrowCircleSvgComponent.jsx';

export default class HdgComponent extends Component {
  render() {
    let total_width = 256;
    let total_height = 256;
    let watchface_padding = 24;
    let arrow_width = 32;
    let north_arrow_width = 14;
    let cardinal_direction = (
      <text x={122} y={20} className="blksail-active-text">
        N
      </text>
    );
    if (this.props.sml === 's') {
      total_width = 80;
      total_height = 80;
      watchface_padding = 8;
      arrow_width = 20;
      north_arrow_width = 8;
      cardinal_direction = <text></text>;
    } else if (this.props.sml === 'l') {
      total_width = 512;
      total_height = 512;
      watchface_padding = 48;
      arrow_width = 64;
      north_arrow_width = 22;
      let character_width = 12;
      cardinal_direction = (
        <g>
          <text x={total_width / 2 - character_width / 2} y={watchface_padding - 4} className="blksail-active-text">
            N
          </text>
          <text x={total_width - watchface_padding + 2} y={262} className="blksail-active-text">
            E
          </text>
          <text x={total_width / 2 - character_width / 2} y={480} className="blksail-active-text">
            S
          </text>
          <text x={30} y={262} className="blksail-active-text">
            W
          </text>
        </g>
      );
    }
    const viewBox = '0 0 ' + total_width + ' ' + total_height;
    let arrow_rotation = 0;
    let frame_rotation = 0;
    if (this.props.rotate_frame) {
      frame_rotation = -this.props.heading_degrees;
    } else {
      arrow_rotation = this.props.heading_degrees;
    }
    let heading_arrow_rotate_string = 'rotate(' + arrow_rotation + ' ' + total_width / 2 + ' ' + total_height / 2 + ')';
    let frame_rotate_string = 'rotate(' + frame_rotation + ' ' + total_width / 2 + ' ' + total_height / 2 + ')';

    return (
      <svg x={0} y={0} width={total_width} height={total_height} viewBox={viewBox}>
        <g transform={frame_rotate_string}>
          <AzimuthWatchFaceSvgComponent x={watchface_padding} y={watchface_padding} sml={this.props.sml} />
          <HdgNorthArrowSvgComponent
            x={total_width / 2 - north_arrow_width / 2}
            y={watchface_padding}
            sml={this.props.sml}
          ></HdgNorthArrowSvgComponent>
          {cardinal_direction}
        </g>
        <g transform={heading_arrow_rotate_string}>
          <HdgArrowSvgComponent x={total_width / 2 - arrow_width / 2} y={0} sml={this.props.sml} />
        </g>
        <HdgArrowCircleSvgComponent x={total_width / 2 - 8} y={total_height / 2 - 8} sml={this.props.sml} />
      </svg>
    );
  }
}

HdgComponent.defaultProps = {};

HdgComponent.propTypes = {
    heading_degrees: PropTypes.number,
    /**
     * Rotate the frame or the compass needle
     * If true the frame is rotate and the needle is up
     * If false the needle is rotate and the north in the frame is up
     */
    rotate_frame: PropTypes.bool,
    /**
     * Size of the component: 's', 'm', 'l'
     */
    sml: PropTypes.string,
    /**
     * Dash-assigned callback that should be called to report property changes
     * to Dash, to make them available for callbacks.
     */
    setProps: PropTypes.func
};
